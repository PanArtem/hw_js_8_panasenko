let formForInput = document.getElementById('formFoPrice'),
    inputPrise = document.getElementById('inputPrice'),
    spanCurrentPrice = document.getElementById('currentPrice'),
    spanCorrectPrice = document.getElementById('correctPrice'),
    borderSpanCurrentPrise = spanCurrentPrice,
    borderSpanCorrectPrise = spanCorrectPrice;


let resetCorrectPrice = document.createElement('span');
    formForInput.insertBefore(resetCorrectPrice, formForInput.children[1]);
    resetCorrectPrice.style.cssText = 'display: none;' +
        'padding-right: 5px;' +
        'padding-left: 5px;' +
        'padding-bottom: 2px;' +
        'border: 0.5px solid #4D56F7;' +
        'border-radius: 50%;' +
        'position: absolute;' +
        'left: 254px;' +
        'top: 15px;';

inputPrise.onblur = () => {
    let valuePrise = inputPrise.value;
    if (+valuePrise > 0 || +valuePrise === 0) {
        spanCurrentPrice.innerHTML = `Текущая цена: ${valuePrise}`;
        spanCorrectPrice.innerHTML = '';
        resetCorrectPrice.innerHTML = 'x';
        resetCorrectPrice.style.display = 'inline-block';
        borderSpanCurrentPrise.style.cssText = 'border: 1px solid #4D56F7;' +
            'border-radius: 10px;' +
            'padding-left: 5px;' +
            'padding-top: 2px;';
        borderSpanCorrectPrise.style.border = '';
        resetCorrectPrice.style.fontSize ='10px' ;
        inputPrise.style.backgroundColor = '#84F779'
    } else {
        spanCorrectPrice.innerHTML = 'Please enter correct price ';
        spanCurrentPrice.innerHTML = '';
        resetCorrectPrice.innerHTML = '';
        resetCorrectPrice.style.display = 'none';
        borderSpanCorrectPrise.style.cssText = 'border: 1px solid gray;' +
            'border-radius: 10px;' +
            'padding-left: 5px;' +
            'padding-top: 2px;';
        borderSpanCurrentPrise.style.border = '';
        inputPrise.style.backgroundColor = '#F7816F';
    }
};

resetCorrectPrice.onclick = () => {
    spanCurrentPrice.innerHTML = '';
    resetCorrectPrice.innerHTML = '';
    resetCorrectPrice.style.display = 'none';
    borderSpanCurrentPrise.style.border = '';
    inputPrise.value = '';
};


